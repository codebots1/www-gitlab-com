---
layout: markdown_page
title: "FY22-Q4 OKRs"
description: "View GitLabs Objective-Key Results for FY22 Q4. Learn more here!"
canonical_path: "/company/okrs/fy22-q4/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from November 1, 2021 to January 31, 2022.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2021-09-27 | CEO shares top goals with E-group for feedback |
| -5 | 2021-09-27 | CEO pushes top goals to this page |
| -4 | 2021-10-04 | E-group propose OKRs for their functions in [Epics and Issues nested under the CEO's OKRs](/company/okrs/#executives-propose-okrs-for-their-functions). These issues and epics are shared in #okrs Slack channel |
| -3 | 2021-10-11 | E-group 50 minute draft review meeting |
| -2 | 2021-10-18 | E-group discusses with their respective teams and polish their OKRs |
| -1 | 2021-10-25 | CEO reports post links to final OKR Epics in #okrs slack channel and @ mention the CEO and CoS for approval |
| 0  | 2021-11-01 | CoS updates OKR page for current quarter to be active |


## OKRs

### 1. CEO


### 2. CEO


### 3. CEO

