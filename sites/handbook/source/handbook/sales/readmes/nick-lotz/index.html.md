---
layout: markdown_page
title: "Nick Lotz's README"
---

<!-- This template will help you build out your very own GitLab README, a great tool for transparently letting others know what it's like to work with you, and how you prefer to be communicated with. Each section is optional. You can remove those you aren't comfortable filling out, and add sections that are germane to you. --> 

## Nick Lotz's README

I'm Nick, a technical instructor on the Professional Services team. This page is intended to be a pesonal introduction, as well as help others understand what it might be like to work with me.

Please feel free to contribute to this page by opening a merge request. 

## About me

My academic background is in chemical engineering, but I've spent my career in tech, and have worked in technical training roles since 2017.

I currently live in Nashville, Tennessee, USA, and am fortunate to be near family and close friends. My household includes me and my rescued lab [Jet](https://about.gitlab.com/company/team-pets/#304-jet).

I'm a huge soccer (football) fan, having grown up playing the game and refereeing as an adult. I cheer on the L.A. Galaxy and U.S. national teams, but follow most worldwide leagues. Always down to meet up in person to play a pickup game or watch a match.


## My working style

- I take seriously GitLab's support for the [non-linear workday](https://about.gitlab.com/company/culture/all-remote/non-linear-workday/). When possible I do focused work in the early morning and late evening, while maintaining meetings, training and coffee chats during traditional business hours. 
- Writer's block can be a frequent issue for me. For that reason I strive to create templates and high level outlines for even the smallest projects and [iterate](https://about.gitlab.com/handbook/engineering/workflow/iteration/) later.
- I learn best from books and written documentation, and use [spaced repetition](https://en.wikipedia.org/wiki/Spaced_repetition) a.k.a. flashcards to help commit concepts to memory. 

## Communicating with me

- I value [async](https://about.gitlab.com/company/culture/all-remote/asynchronous/) (Slack, GitLab issue) for most clear action items and high level feedback. Putting everything possible in writing is ideal as I work off written to-do lists.
- But, don't hesitate to schedule a sync up or [coffee chat](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) with me any time there's space in my calendar. I value face-to-face meetings for (1) hashing out ideas, and (2) getting to know you as a person.

## Related pages

[GitLab](https://gitlab.com/nlotz) \
[LinkedIn](https://www.linkedin.com/in/nicholaslotz)

