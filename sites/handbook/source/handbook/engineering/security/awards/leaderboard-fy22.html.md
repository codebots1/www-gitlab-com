---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@tkuah](https://gitlab.com/tkuah) | 1 | 1240 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 2 | 1080 |
| [@sabrams](https://gitlab.com/sabrams) | 3 | 1000 |
| [@manojmj](https://gitlab.com/manojmj) | 4 | 960 |
| [@engwan](https://gitlab.com/engwan) | 5 | 780 |
| [@theoretick](https://gitlab.com/theoretick) | 6 | 700 |
| [@vitallium](https://gitlab.com/vitallium) | 7 | 600 |
| [@leipert](https://gitlab.com/leipert) | 8 | 580 |
| [@mksionek](https://gitlab.com/mksionek) | 9 | 580 |
| [@alexpooley](https://gitlab.com/alexpooley) | 10 | 500 |
| [@pks-t](https://gitlab.com/pks-t) | 11 | 500 |
| [@stanhu](https://gitlab.com/stanhu) | 12 | 480 |
| [@djadmin](https://gitlab.com/djadmin) | 13 | 480 |
| [@10io](https://gitlab.com/10io) | 14 | 410 |
| [@whaber](https://gitlab.com/whaber) | 15 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 16 | 400 |
| [@markrian](https://gitlab.com/markrian) | 17 | 360 |
| [@mrincon](https://gitlab.com/mrincon) | 18 | 340 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 19 | 340 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 20 | 320 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 21 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 22 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 23 | 300 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 24 | 260 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 25 | 250 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 26 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 27 | 200 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 28 | 200 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 29 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 30 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 31 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 32 | 200 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 33 | 200 |
| [@seanarnold](https://gitlab.com/seanarnold) | 34 | 180 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 35 | 140 |
| [@mwoolf](https://gitlab.com/mwoolf) | 36 | 140 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 37 | 140 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 38 | 140 |
| [@dblessing](https://gitlab.com/dblessing) | 39 | 140 |
| [@kerrizor](https://gitlab.com/kerrizor) | 40 | 130 |
| [@twk3](https://gitlab.com/twk3) | 41 | 130 |
| [@nfriend](https://gitlab.com/nfriend) | 42 | 120 |
| [@balasankarc](https://gitlab.com/balasankarc) | 43 | 110 |
| [@allison.browne](https://gitlab.com/allison.browne) | 44 | 100 |
| [@cablett](https://gitlab.com/cablett) | 45 | 100 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 46 | 90 |
| [@vsizov](https://gitlab.com/vsizov) | 47 | 80 |
| [@splattael](https://gitlab.com/splattael) | 48 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 49 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 50 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 51 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 52 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 53 | 80 |
| [@mkozono](https://gitlab.com/mkozono) | 54 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 55 | 80 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 56 | 80 |
| [@acroitor](https://gitlab.com/acroitor) | 57 | 80 |
| [@rcobb](https://gitlab.com/rcobb) | 58 | 80 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 59 | 70 |
| [@mbobin](https://gitlab.com/mbobin) | 60 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 61 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 62 | 60 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 63 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 64 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 65 | 60 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 66 | 60 |
| [@minac](https://gitlab.com/minac) | 67 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 68 | 50 |
| [@tomquirk](https://gitlab.com/tomquirk) | 69 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 70 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 71 | 40 |
| [@dgruzd](https://gitlab.com/dgruzd) | 72 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 73 | 40 |
| [@afontaine](https://gitlab.com/afontaine) | 74 | 40 |
| [@proglottis](https://gitlab.com/proglottis) | 75 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 76 | 30 |
| [@cngo](https://gitlab.com/cngo) | 77 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 78 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 79 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 80 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 81 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 80 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |
| [@rspeicher](https://gitlab.com/rspeicher) | 8 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 600 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@feistel](https://gitlab.com/feistel) | 1 | 800 |
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 2 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 3 | 500 |
| [@behrmann](https://gitlab.com/behrmann) | 4 | 500 |
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 5 | 300 |
| [@tnir](https://gitlab.com/tnir) | 6 | 200 |

## FY22-Q3

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@tkuah](https://gitlab.com/tkuah) | 1 | 1000 |
| [@sabrams](https://gitlab.com/sabrams) | 2 | 700 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 600 |
| [@mksionek](https://gitlab.com/mksionek) | 4 | 120 |
| [@acroitor](https://gitlab.com/acroitor) | 5 | 80 |
| [@rcobb](https://gitlab.com/rcobb) | 6 | 80 |
| [@stanhu](https://gitlab.com/stanhu) | 7 | 80 |
| [@djadmin](https://gitlab.com/djadmin) | 8 | 80 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 9 | 60 |
| [@jivanvl](https://gitlab.com/jivanvl) | 10 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 11 | 60 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 12 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 13 | 60 |
| [@minac](https://gitlab.com/minac) | 14 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 15 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 16 | 60 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 17 | 60 |
| [@afontaine](https://gitlab.com/afontaine) | 18 | 40 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 19 | 30 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 20 | 30 |
| [@10io](https://gitlab.com/10io) | 21 | 30 |
| [@mwoolf](https://gitlab.com/mwoolf) | 22 | 30 |

### Engineering

Category is empty

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@feistel](https://gitlab.com/feistel) | 1 | 800 |
| [@behrmann](https://gitlab.com/behrmann) | 2 | 500 |

## FY22-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@manojmj](https://gitlab.com/manojmj) | 1 | 900 |
| [@pks-t](https://gitlab.com/pks-t) | 2 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 3 | 400 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 4 | 400 |
| [@10io](https://gitlab.com/10io) | 5 | 380 |
| [@leipert](https://gitlab.com/leipert) | 6 | 380 |
| [@markrian](https://gitlab.com/markrian) | 7 | 360 |
| [@ali-gitlab](https://gitlab.com/ali-gitlab) | 8 | 340 |
| [@mksionek](https://gitlab.com/mksionek) | 9 | 320 |
| [@mrincon](https://gitlab.com/mrincon) | 10 | 300 |
| [@thiagocsf](https://gitlab.com/thiagocsf) | 11 | 300 |
| [@mikeeddington](https://gitlab.com/mikeeddington) | 12 | 300 |
| [@theoretick](https://gitlab.com/theoretick) | 13 | 300 |
| [@stanhu](https://gitlab.com/stanhu) | 14 | 300 |
| [@tkuah](https://gitlab.com/tkuah) | 15 | 240 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 16 | 200 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 17 | 120 |
| [@engwan](https://gitlab.com/engwan) | 18 | 100 |
| [@mkozono](https://gitlab.com/mkozono) | 19 | 80 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 20 | 80 |
| [@dblessing](https://gitlab.com/dblessing) | 21 | 80 |
| [@tancnle](https://gitlab.com/tancnle) | 22 | 80 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 23 | 80 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 24 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 25 | 70 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 26 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 27 | 60 |
| [@jerasmus](https://gitlab.com/jerasmus) | 28 | 60 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 29 | 60 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 30 | 60 |
| [@jannik_lehmann](https://gitlab.com/jannik_lehmann) | 31 | 60 |
| [@nfriend](https://gitlab.com/nfriend) | 32 | 60 |
| [@dgruzd](https://gitlab.com/dgruzd) | 33 | 40 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 34 | 40 |
| [@f_caplette](https://gitlab.com/f_caplette) | 35 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 36 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 37 | 30 |
| [@ekigbo](https://gitlab.com/ekigbo) | 38 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 39 | 30 |
| [@lulalala](https://gitlab.com/lulalala) | 40 | 30 |
| [@dzaporozhets](https://gitlab.com/dzaporozhets) | 41 | 30 |
| [@ifarkas](https://gitlab.com/ifarkas) | 42 | 30 |
| [@nmezzopera](https://gitlab.com/nmezzopera) | 43 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rspeicher](https://gitlab.com/rspeicher) | 1 | 30 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 2 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@lienvdsteen](https://gitlab.com/lienvdsteen) | 1 | 600 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@JeremyWuuuuu](https://gitlab.com/JeremyWuuuuu) | 1 | 600 |
| [@leetickett](https://gitlab.com/leetickett) | 2 | 500 |
| [@tnir](https://gitlab.com/tnir) | 3 | 200 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 1 | 1000 |
| [@engwan](https://gitlab.com/engwan) | 2 | 680 |
| [@alexpooley](https://gitlab.com/alexpooley) | 3 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 4 | 400 |
| [@whaber](https://gitlab.com/whaber) | 5 | 400 |
| [@sabrams](https://gitlab.com/sabrams) | 6 | 300 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 7 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 8 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 9 | 250 |
| [@leipert](https://gitlab.com/leipert) | 10 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 11 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 12 | 200 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 13 | 200 |
| [@mattkasa](https://gitlab.com/mattkasa) | 14 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 15 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 16 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 17 | 140 |
| [@twk3](https://gitlab.com/twk3) | 18 | 130 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 19 | 110 |
| [@balasankarc](https://gitlab.com/balasankarc) | 20 | 110 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 22 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 23 | 100 |
| [@cablett](https://gitlab.com/cablett) | 24 | 100 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 25 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 26 | 80 |
| [@splattael](https://gitlab.com/splattael) | 27 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 28 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 29 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 30 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 31 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 32 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 33 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 34 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 35 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 36 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 37 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 38 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 39 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 40 | 60 |
| [@pursultani](https://gitlab.com/pursultani) | 41 | 50 |
| [@tomquirk](https://gitlab.com/tomquirk) | 42 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 43 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 44 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 45 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 46 | 40 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 47 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 48 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 49 | 30 |
| [@cngo](https://gitlab.com/cngo) | 50 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@mayra-cabrera](https://gitlab.com/mayra-cabrera) | 5 | 50 |
| [@smcgivern](https://gitlab.com/smcgivern) | 6 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 7 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


